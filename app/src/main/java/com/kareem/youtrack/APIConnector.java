package com.kareem.youtrack;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kareem on 11/11/15.
 */
public class APIConnector {

    RequestQueue queue;
    //final String apiUrl = "http://youtrack-dev.elasticbeanstalk.com";
    final String apiUrl = "http://192.168.0.223:3000";



    public APIConnector(Context context){
        queue = Volley.newRequestQueue(context);

    }

    public void APIauthenticate(final String email, final String password, final Context context){
        String url = apiUrl + "/authenticate";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String res) {
                        Log.d("RESPONSE", res);
                        SharedPreferences settings = context.getSharedPreferences("settings", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        try{
                            JSONObject jsonResponse= new JSONObject(res);
                            if(jsonResponse.getBoolean("success") == false){
                                editor.putString("token", "ERROR");
                                editor.commit();
                                Log.d("RESPONSE", "ERROR");
                            }
                            else{
                                editor.putString("token", jsonResponse.getString("token"));
                                editor.commit();
                                Log.d("RESPONSE", "Saved token");
                            }

                        }catch (JSONException jsonEx){
                            jsonEx.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String>getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", email);
                        params.put("password", password);
                        params.put("mobile", "true");
                        return params;
                    }
        };
        queue.add(request);
    }

    public void getVehicles(String token){
        String path = apiUrl + "/vehicles?token=%s";
        String url = String.format(path, token);
//        String url = apiUrl + "/vehicles";
        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String res) {
                        Log.d("RESPONSE", res);
                        try{
                            JSONObject jsonResponse= new JSONObject(res);
                            System.out.println(jsonResponse);
                            VolleyLog.v("json response: %s", jsonResponse);
                        }catch (JSONException jsonEx){
                            jsonEx.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
//        {
//                    @Override
//                    protected Map<String, String>getParams() throws AuthFailureError{
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("token", token);
//                        return params;
//                    }
//        };
        queue.add(request);
    }

//    public void sendAccReadings(int vehilce_id, JSONArray jsonArray){
//        String path = apiUrl + "/vehicle/accreadings/?token=%s";
//        String url = String.format(path, token);
//        JSONObject jsonReadingsRequest = new JSONObject();
//        try {
//            jsonReadingsRequest.put("vehicle", vehilce_id);
////            JSONObject json = new JSONObject();
////            json.put("x", 1.2);
////            json.put("y", 1.2);
////            json.put("z", 1.2);
////            json.put("timestamp", "2015-1-2 08:08:00");
////            jsonReadingsRequest.accumulate("readings", json);
////            JSONObject json1 = new JSONObject();
////            json.put("x", 1.3);
////            json.put("y", 1.3);
////            json.put("z", 1.3);
////            json.put("timestamp", "2012-1-2 08:08:00");
////            jsonReadingsRequest.accumulate("readings", json1 );
//            jsonReadingsRequest.put("readings", jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest request = new JsonObjectRequest( url, jsonReadingsRequest,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject res) {
//                        Log.d("RESPONSE", res.toString());
//                        try{
//                            JSONObject jsonResponse= new JSONObject(res.toString());
//                            System.out.println(jsonResponse);
//                            VolleyLog.v("json response: %s", jsonResponse);
//                        }catch (JSONException jsonEx){
//                            jsonEx.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        error.printStackTrace();
//                    }
//                });
//        queue.add(request);
//    }

    public void sendReadings(int vehilce_id, String url_part, JSONArray jsonArray, String token) {
        String path = apiUrl + "/vehicle/" + url_part + "/?token=%s";
        String url = String.format(path, token);
        JSONObject jsonReadingsRequest = new JSONObject();
        try {
            jsonReadingsRequest.put("vehicle", vehilce_id);
            jsonReadingsRequest.put("readings", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(url, jsonReadingsRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject res) {
                        Log.d("RESPONSE", res.toString());
                        try {
                            JSONObject jsonResponse = new JSONObject(res.toString());
                            System.out.println(jsonResponse);
                            VolleyLog.v("json response: %s", jsonResponse);
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        queue.add(request);
    }
}
