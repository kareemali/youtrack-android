package com.kareem.youtrack;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements SensorEventListener
        ,GoogleApiClient.ConnectionCallbacks
        ,GoogleApiClient.OnConnectionFailedListener
        ,LocationListener{
    TextView accView;
    TextView gpsView;
    TextView gyroView;
    private SensorManager mSensorManager;
    private Sensor accSensor;
    private Sensor gyroSensor;

    //last saved updates
    float[]acc_last_update = {0,0,0};
    float[]gyro_last_update = {0,0,0};
    double []gps_last_update = {0,0,0};

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private String mLastUpdateTime;
//    private RequestingLocationUpdates mRequestingLocationUpdates;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final String TAG = MainActivity.class.getSimpleName();

    DatabaseHelper mDbHelper = new DatabaseHelper(this);
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    double accError = 1.0;
    double gyroError = 1.0;
    double gpsError = 1.0;

    String token;// = (String) getSharedPreferences("settings", 0).getString("token", "FALSE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(checkPlayServices()){
            buildGoogleApiClient();
        }

        APIConnector apiConnector = new APIConnector(this);
//        apiConnector.APIauthenticate();
//        apiConnector.getVehicles();


        dbWriter = mDbHelper.getWritableDatabase();
        dbReader = mDbHelper.getReadableDatabase();

        apiConnector.sendReadings(1, "accreadings", pullDBandDrop(SensorDataContract.AccelerometerReading.TABLE_NAME), token);
        apiConnector.sendReadings(1, "gyroreadings", pullDBandDrop(SensorDataContract.GyroReading.TABLE_NAME), token);
        apiConnector.sendReadings(1, "gpsreadings", pullDBandDropGPS(), token);

        accView = (TextView) findViewById(R.id.accView);
        gpsView = (TextView) findViewById(R.id.gpsView);
        gyroView = (TextView) findViewById(R.id.gyroView);


        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_NORMAL);

        gyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mSensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);

        createLocationRequest();

    }

    public JSONArray pullDBandDrop(String tableName){
        JSONArray result = new JSONArray();
        String query = "SELECT * FROM " + tableName;
        Cursor cursor =  dbReader.rawQuery(query, null);
        if(cursor.moveToFirst()){
            JSONObject jo = new JSONObject();
            try {
                jo.put("x", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_X_VALUE)));
                jo.put("y", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_Y_VALUE)));
                jo.put("z", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_Z_VALUE)));
                jo.put("timestamp", cursor.getString(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_CREATED_AT)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            result.put(jo);
            while(cursor.moveToNext()){
                JSONObject jo1 = new JSONObject();
                try {
                    jo1.put("x", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_X_VALUE)));
                    jo1.put("y", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_Y_VALUE)));
                    jo1.put("z", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_Z_VALUE)));
                    jo1.put("timestamp", cursor.getString(cursor.getColumnIndex(SensorDataContract.AccelerometerReading.COLUMN_NAME_CREATED_AT)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                result.put(jo1);
            }
        }
        dbWriter.execSQL("DELETE FROM " + tableName);
        return result;
    }

    public JSONArray pullDBandDropGPS(){
        JSONArray result = new JSONArray();
        String query = "SELECT * FROM " + SensorDataContract.GpsReading.TABLE_NAME;
        Cursor cursor =  dbReader.rawQuery(query, null);
        if(cursor.moveToFirst()){
            JSONObject jo = new JSONObject();
            try {
                jo.put("latitude", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_LATITUDE)));
                jo.put("longitude", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_LONGITUDE)));
                jo.put("timestamp", cursor.getString(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_CREATED_AT)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            result.put(jo);
            while(cursor.moveToNext()){
                JSONObject jo1 = new JSONObject();
                try {
                    jo1.put("latitude", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_LATITUDE)));
                    jo1.put("longitude", cursor.getDouble(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_LONGITUDE)));
                    jo1.put("timestamp", cursor.getString(cursor.getColumnIndex(SensorDataContract.GpsReading.COLUMN_NAME_CREATED_AT)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                result.put(jo1);
            }
        }
        dbWriter.execSQL("DELETE FROM " + SensorDataContract.GpsReading.TABLE_NAME);
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkWithinError(float []values,  String sensor){
        float [] sensor_last_update  = {0,0,0};
        double error = 0;
        switch(sensor){
           case "acc": sensor_last_update = acc_last_update;
              error = accError;
               break;
           case "gyro": sensor_last_update = gyro_last_update;
               error = gyroError;
               break;
        }
        for(int i = 0; i<values.length; i++){
            if(Math.abs(values[i]- sensor_last_update[i])> error){
               return false;
            }
        }
        return true;
    }
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if(mySensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
            if(!checkWithinError(sensorEvent.values, "acc")){
                accView.setText("");
                accView.append("x: " + sensorEvent.values[0]);
                accView.append("\ny: "+ sensorEvent.values[1]);
                accView.append("\nz: " + sensorEvent.values[2]);
                acc_last_update[0] = sensorEvent.values[0];
                acc_last_update[1] = sensorEvent.values[1];
                acc_last_update[2] = sensorEvent.values[2];
                insertReading(SensorDataContract.AccelerometerReading.COLUMN_NAMES, sensorEvent, SensorDataContract.AccelerometerReading.TABLE_NAME);
            }
        }

        if(mySensor.getType() == Sensor.TYPE_GYROSCOPE){
            if(!checkWithinError(sensorEvent.values, "gyro")){
                gyroView.setText("");
                gyroView.append("around x: " + sensorEvent.values[0]);
                gyroView.append("\naround y: " + sensorEvent.values[1]);
                gyroView.append("\naround z: " + sensorEvent.values[2]);
                gyro_last_update[0] = sensorEvent.values[0];
                gyro_last_update[1] = sensorEvent.values[1];
                gyro_last_update[2] = sensorEvent.values[2];
                insertReading(SensorDataContract.GyroReading.COLUMN_NAMES, sensorEvent, SensorDataContract.GyroReading.TABLE_NAME);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean checkPlayServices(){
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLastLocation != null){
            gpsView.append("\nLatitude: " + String.valueOf(mLastLocation.getLatitude()));
            gpsView.append("\nLongitude: " + String.valueOf(mLastLocation.getLongitude()));
        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    protected void createLocationRequest(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates(){
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        mLastUpdateTime = sdf.format(dt);

        //mLastUpdateTime = DateFormat.getDateTimeInstance().format(new Date());
//        mLastUpdateTime = (new Date()).getTime() + (sensorEvent.timestamp - System.nanoTime()) / 1000000L;

        //updating UI and
        //update db
        if(Math.abs(location.getLatitude() - gps_last_update[0])>gyroError || Math.abs(location.getLongitude() - gps_last_update[1])>gyroError){
            gpsView.setText("");
            gpsView.append("Latitude: " + String.valueOf(mLastLocation.getLatitude()));
            gpsView.append("\nLongitude : " + String.valueOf(mLastLocation.getLongitude()));
            gpsView.append("\nSpeed: "+ String.valueOf(mLastLocation.getSpeed()));
            gps_last_update[0] = location.getLatitude();
            gps_last_update[1] = location.getLongitude();
            insertReading(location, mLastUpdateTime);
        }
        Log.i(TAG, "UPDATED DATABASE");

    }

    protected void stopLocationUpdates(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public long insertReading(String[] columns, SensorEvent event, String tableName){
        ContentValues values = new ContentValues();
        for (int i = 0; i<event.values.length ; i++){
            values.put(columns[i], event.values[i]);
        }
        long timeInMillis = (new Date()).getTime()
                + (event.timestamp - System.nanoTime()) / 1000000L;
//        values.put("created_at", event.timestamp);
//        values.put("created_at", timeInMillis);
        //add values in iso for now
        //values.put("created_at", DateFormat.getDateTimeInstance().format(new Date()));
        //Log.v("DateFormat", "***********************************");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        Date dt = new Date();
        String dateString = sdf.format(dt);
        //Log.v("DateFormat",dateString);
        values.put("created_at", dateString);
        //values.put("created_at", DateFormat.getDateTimeInstance().format("yyyy-mm-hh hh:mm:ss"));
        return dbWriter.insert(tableName, null, values);
    }

    public long insertReading( Location location ,String timeStamp){
        ContentValues values = new ContentValues();
        values.put(SensorDataContract.GpsReading.COLUMN_NAME_LATITUDE ,location.getLatitude());
        values.put(SensorDataContract.GpsReading.COLUMN_NAME_LONGITUDE,location.getLongitude());
        values.put(SensorDataContract.GpsReading.COLUMN_NAME_SPEED, location.getSpeed());
        values.put(SensorDataContract.GpsReading.COLUMN_NAME_CREATED_AT, timeStamp);
        return dbWriter.insert(SensorDataContract.GpsReading.TABLE_NAME, null, values);
    }
}