package com.kareem.youtrack;

import android.provider.BaseColumns;

/**
 * Created by kareem on 10/21/15.
 */
public class SensorDataContract {
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "sensordata.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private SensorDataContract(){}

    public static abstract class AccelerometerReading implements BaseColumns {
        public static final String TABLE_NAME = "accelerometerReading";
        public static final String COLUMN_NAME_X_VALUE = "xValue";
        public static final String COLUMN_NAME_Y_VALUE = "yValue";
        public static final String COLUMN_NAME_Z_VALUE = "zValue";
        public static final String COLUMN_NAME_CREATED_AT = "created_at";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + "_ID" + " INTERGER PRIMARY KEY,"
                + COLUMN_NAME_X_VALUE + " DOUBLE,"
                + COLUMN_NAME_Y_VALUE + " DOUBLE,"
                + COLUMN_NAME_Z_VALUE + " DOUBLE,"
                + COLUMN_NAME_CREATED_AT + " DATETIME)";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String[] COLUMN_NAMES = {COLUMN_NAME_X_VALUE, COLUMN_NAME_Y_VALUE, COLUMN_NAME_Z_VALUE, COLUMN_NAME_CREATED_AT};
    }




    public static abstract class GpsReading implements BaseColumns{
        public static final String TABLE_NAME = "gpsReading";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "logitude";
        public static final String COLUMN_NAME_SPEED = "speed";
        public static final String COLUMN_NAME_CREATED_AT = "created_at";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + "_ID" + " INTERGER PRIMARY KEY,"
                + COLUMN_NAME_LATITUDE + " DOUBLE,"
                + COLUMN_NAME_LONGITUDE + " DOUBLE,"
                + COLUMN_NAME_SPEED + " DOUBLE,"
                + COLUMN_NAME_CREATED_AT + " DATETIME)";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String[] COLUMN_NAMES = {COLUMN_NAME_LATITUDE, COLUMN_NAME_LONGITUDE, COLUMN_NAME_CREATED_AT };
    }

    public static abstract class GyroReading implements BaseColumns{
        public static final String TABLE_NAME = "gyroReading";
        public static final String COLUMN_NAME_X_VALUE = "xValue";
        public static final String COLUMN_NAME_Y_VALUE = "yValue";
        public static final String COLUMN_NAME_Z_VALUE = "zValue";
        public static final String COLUMN_NAME_CREATED_AT = "created_at";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + "_ID" + " INTERGER PRIMARY KEY,"
                + COLUMN_NAME_X_VALUE + " DOUBLE,"
                + COLUMN_NAME_Y_VALUE + " DOUBLE,"
                + COLUMN_NAME_Z_VALUE + " DOUBLE,"
                + COLUMN_NAME_CREATED_AT + " DATETIME)";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String[] COLUMN_NAMES = {COLUMN_NAME_X_VALUE, COLUMN_NAME_Y_VALUE, COLUMN_NAME_Z_VALUE, COLUMN_NAME_CREATED_AT};

    }
}
