package com.kareem.youtrack;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.hardware.SensorManager;

/**
 * Created by kareem on 10/21/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(Context context){
        super(context, SensorDataContract.DATABASE_NAME,null, SensorDataContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
//        System.out.println("hoppa hoppa");
//        System.out.println(SensorDataContract.AccelerometerReading.CREATE_TABLE);
        sqLiteDatabase.execSQL(SensorDataContract.AccelerometerReading.CREATE_TABLE);
        sqLiteDatabase.execSQL(SensorDataContract.GpsReading.CREATE_TABLE);
        sqLiteDatabase.execSQL(SensorDataContract.GyroReading.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SensorDataContract.AccelerometerReading.DELETE_TABLE);
        sqLiteDatabase.execSQL(SensorDataContract.GpsReading.DELETE_TABLE);
        sqLiteDatabase.execSQL(SensorDataContract.GyroReading.DELETE_TABLE);
        onCreate(sqLiteDatabase);
    }

}
